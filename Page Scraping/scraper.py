"""Scrape an EK OP member page for award data"""

def scrapeEK(opid):
    from gazpacho import get, Soup
    url = "https://op.eastkingdom.org/op_ind.php?atlantian_id={}".format(opid)
    html = get(url)
    soup = Soup(html)
    table = soup.find("table", {'summary': "information"})
    title = soup.find("p", {'class': 'title3'})
    header = table.find("th", {'class': 'title'})
    rows = table.find("tr")
    
    # Extract the individual's name
    person = title.text
    
    # extract the data keys from the table header
    dataheader = []
    i = 0
    while i < len(header):
        try:
            text = header[i].text
        except:
            try:
                text = header[i].find("a").text
            except:
                sub = header[i].find("td")
                text = sub[0].find("a").text
        dataheader.append(text)
        i = i + 1    
    
    # Extract the award records from the table
    datarows = []
    i = 0
    while i < len(rows):
        cells = rows[i].find("td", {'class': 'data'})
        m = []
        try: 
            j = 0
            l = len(cells)
            while j < l:
                cell = cells[j]
                text = cell.strip()
                m.append(text)
                j = j + 1
        except:
            pass
        else:
            #datarows.append(m)
            dictionary = dict(zip(dataheader, m))
            datarows.append(dictionary)
        i = i + 1
    # New key-value pair with person's name as the key and the awards dataset as the value
    record = {person : datarows}
    
    # add a very short delay so we're making a new request no more frequently than every 3 seconds
    import time
    time.sleep(3)
    
    return record