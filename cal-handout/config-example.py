""" Store external variables for the calendar handout builder script """

config = {
    # Calendar URLs
    "cal1": "url for first calendar",
    "cal2": "url for second calendar",
    # User-agent string
    "user_agent": "put your app's name here",
    # Event categories to filter on. Use lower case. Events matching any entry in this list will be included.
    "categories": [
        "one", 
        "two", 
        "three", 
        "four", 
        "five", 
        "six", 
        "seven", 
        "eight",
        "nine",
        "ten", 
        "eleven", 
        "twelve", 
        "thirteen", 
        "fourteen"
    ],
    # Any events that recur with a predictable pattern go here
    "recurring": [
     {
        'name': 'event name',
        'date': 'recurrence pattern, e.g. "every Tuesday at 4pm"',
        'location': 'Street address or other location'
     },
     {
         'name': 'event name',
         'date': 'recurrence pattern',
         'location': 'street address or other location'},
     {
         'name': 'event name',
         'date': 'recurrence pattern',
         'location': 'street address or other location'
     }
    ],
    # the heading and subheading for your markdown document. Preserve the '\n\n' double newlines
    "heading": "# Local & Regional Upcoming Events\n\n",
    "subheading": "More details available at \n\n- (url) and \n- (url)\n\n",
        
}