#!/usr/bin/env python
# coding: utf-8

# In[ ]:


""" Given two iCal endpoint urls, produce a listing of upcoming events """


# In[ ]:


# prerequisites - only run the first time you are running this notebook in a new instance
#get_ipython().system('pip install -U icalevents')


# In[1]:


# library imports
import requests
from datetime import datetime, timedelta, timezone, date
import pytz
from icalevents import icalparser
from config import config


# In[2]:


# get the calendar URLs
cal1_url = config['cal1']
cal2_url = config['cal2']
headers = {'user-agent': config['user_agent']}
cal1 = requests.get(cal1_url, headers=headers).text
cal2 = requests.get(cal2_url, headers=headers).text


# In[3]:


# get the filter categories
cats = config["categories"]


# In[4]:


# set up date & time stuff (choose only one of these)

# if you want the list to start from today
startdate = datetime.now(pytz.timezone('America/New_York'))

# if you want the list to start from a date in the future - specify the date in the `datetime` function call
#startdate = datetime(2024,6,3, hour=9, minute=0, second=0, tzinfo=pytz.timezone('America/New_york'))


# In[25]:


# walk the calendars and add events from both
upcoming = []
# calendar the first
events1 = icalparser.parse_events(cal1, sort=True, start=startdate, default_span=timedelta(days=180))
for event in events1:
    if not event.recurring:
        upcoming.append(event)

# calendar the second
events2 = icalparser.parse_events(cal2, sort=True, start=startdate, default_span=timedelta(days=180))
for event in events2:
    for cat in event.categories:
        if cat.lower() in cats:
            upcoming.append(event)


# In[26]:


# Sort the combined events by start date
upcoming.sort(key=lambda x: x.start)


# In[27]:
updatedate = startdate.strftime('%B %d, %Y')

# write our Markdown file
with open("events.md", "w") as file:
# Write the document heading section
    file.write("---\n")
    file.write("papersize: a4\n")
    file.write("geometry: margin=0.5in\n")
    file.write("pagestyle: empty\n")
    file.write("documentclass: extarticle\n")
    file.write("fontsize: 14pt\n")
    file.write("---\n")
    file.write(config["heading"])
    file.write("*Last Updated {}*\n\n".format(updatedate))
    file.write("***\n\n")
    file.write(config["subheading"])
# Write list of recurring local items
    file.write("## Recurring Events\n")
    for item in config["recurring"]:
        file.write("- {} - {}\n".format(item["name"], item["date"]))
        file.write("  - {}\n".format(item["location"]))

    file.write("\n***\n\n")
    # Print list of upcoming non-recurring events
    file.write("## Upcoming Events\n")
    # limit the number of events so we don't overflow onto a second page
    limit = upcoming[:20]
    for event in limit:
        summary = event.summary.strip()
        if event.location == None:
            event.location = "See details in online calendar"
        if event.location != "See details in online calendar":
   
            event.location = " ".join(event.location.split())
            # Trim location so it's just the street address
            #if event.location[0].isalpha():
                #temp = 0
                # find the first numeric character and keep it and everything after it
                #for chr in event.location:
                    #if chr.isdigit():
                        #temp = event.location.index(chr)
                        #event.location = str(event.location[temp:])
                        #break
            #address = pyap.parse(event.location, country="US")
            #if address:
                #a = address[0]
                #event.location = a.full_address
        # if the event is on a single day, only display one date
        if event.end - event.start == (timedelta(hours=24)):
            file.write("- {} - {}\n".format(summary, event.start.strftime("%b %d %Y")))
        # otherwise, display the start and end dates
        else:
            file.write("- {} - {} to {}\n".format(summary, event.start.strftime("%b %d %Y"), event.end.strftime("%b %d %Y")))
        file.write("  - {}\n".format(event.location))    


# In[ ]:


# From this point, consult the README

