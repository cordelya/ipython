# Printable Upcoming Events Handout

Given two iCal endpoints, filter and combine events into one list sorted by start date, and write the events to a markdown file for conversion to PDF and subsequent printing. Intended for printing two handouts per US Letter page.

This script was written to pull non-recurring events from cal1 and events filtered by category from cal2. If your calendars are not set up that way, you will need to edit the script to handle them.

## Edit the config file

You'll need to specify 

* your iCal endpoint urls 
* a user-agent string for when the script requests your calendars (some iCal endpoints require a user-agent string)
* your event categories to filter on 
* the recuring events the script filters out to avoid repetition (instead you provide the recurrence pattern)
* the heading and subheading of your handout sheet. Links to the online versions of the calendars go in the subheading 

## Run the Script

`python3 handout-builder.py`

If all goes well, it will output a file, `events.md`, into the current working directory

## Convert to PDF and duplicate for 2-up printing 

```sh
pandoc -f markdown events.md -o events.pdf
pdftk events.pdf events.pdf cat output events-double.pdf
```

## Print to 2-up file

In a graphical file manager, open events-double.pdf and initiate the print dialogue.
Print to file (PDF), 2 pages per sheet, output page size A3, fit to printable area.

## Print A3 PDF to Letter PDF file

Otherwise, the printer may complain and spit out a blank page and an error page
with every copy of the actual file.

Print to file (pdf),  1 page per sheet, output size US Letter, fit to printable area.

## Send resulting PDF to printer

Print 1 page per sheet, US Letter size, fit to printable area.
