""" Store external variables for these notebooks to use """

secrets = {
    
    # [Calendar Setup]
    # the url to the ICS endpoint you want to read
    "ical_url": "",
    # The text you want to use for the link to the online calendar
    
    # [Discord Webhook]
    # the Discord webhook for posting to Discord
    "discord_webhook": "https://discord.com/api/webhooks/webhook-id/path",
    
    # [Send Email via Gmail]
    # Your Gmail App Password
    "gmail_app_pass": "16 character app pass from google",
    "gmail_login_address": "the address you use to log in",
    "gmail_from": "an email address you can send email from using the login address",
    "gmail_to": "the address you're sending email to",

    # [Huginn Webhook]
    # The Huginn Webhook for integrating with Huginn Agents
    "huginn_webhook": "",
}
# [keywords]

# keywords you might use in an event summary (aka the event name)
# single-word keywords are better here because the matching will match 
# against the entire keyword string. The matching algo is case-insensitive
# but the keywords are not converted so put them here in all lower-case
subj_keys = ['keyword', 'keyword', 'keyword', 'keyword', 'keyword']

# same as above, but for the event location field. You may wish to include
# both the name of a location (business name, name of home-owner, etc) and
# also include the street address (only the street address without the city
# state and zip may get you a few false positives but 
loc_keys = ["location name", "address"]