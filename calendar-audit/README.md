# iCal Calendar Auditor

## Intro

I had a problem: I made an appointment for vehicle repair with a local shop. The appointment was the type where you drop off the vehicle and return later to collect it once the work is done. The vehicle was supposed to have been dropped off the day before (a Sunday), only that didn't happen! I had set a notification for the day prior, and the notification was received, but I incorrectly assumed that the appointment was a timed ("wait in the waiting room") appointment, so I said, "Yes, thank you." and dismissed the notification. Oops. We ended up delivering the vehicle to the shop around 11:30 on the day of the appointment. Less than ideal for everyone involved.

## Good Systems Don't Allow User Failure

So how do we prevent this from occurring in the future? I can try to remember to include "drop off" in the Event Summary as a way of forcing my brain to make a meaningful connection between a day-before reminder and the fact that I need to take action on it *now*.

But how do I make sure that I consistently do that, especially for a type of event that occurs only a few times each year? Because I'm me, my answer to that was to write an auditor script in Python that will check upcoming appointments on my calendar and alert me whenever an appointment summary includes "vehicle appointment" keywords in the Event Summary or Location, AND doesn't include "drop off" in the Event Summary.

## Make this Script Your Own

This script can be adapted to other kinds of appointments and I've attempted to make customizing it somewhat easy by removing the event summary and location keyword lists to the 'secrets' file.

It also should not be terribly difficult to expand to make it check for two different scenarios. Perhaps I want to *also* get alerts when the event summary includes the word "dental", the location includes my dentist's office address, and the event duration is 24 hours (indicative of an all-day event with no explicit start/end time) in case I forget to input the time of my next appointment - now I have time to call the office and ask them to confirm what time the appointment is for.

I made this script send an alert via Huginn, but there's scaffolding in the secrets file for sending an alert via gmail or to a Discord channel (where you have admin access because it requires setting up a bot and posting via that bot's webhook). 

### Post to a Discord Channel

```python
# Imports
import json, os

# Create the message
text = "foo"

# Give it some structure
discord_payload = {'content': text}

# Turn it into json
json_payload = json.dumps(discord)

# Send it to Discord
url = secrets.secrets['discord_webhook']
os.system("curl -H 'Content-type: application/json' -d '{}'".format(json_payload.replace("'", r"'\''"), url))
```
### Send via Email

```python
# Imports
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
import smtplib

# Init the email message
msg = MIMEMultipart('alternative')
msg['Subject'] = "Email subject"
msg['From'] = secrets.secrets['gmail_from']
msg['to'] = secrets.secrets['gmail_to']

# Compose the email body

# The plaintext version
text = "Some text here. Use \n to insert newlines and \t to insert tabs. No other formatting is supported for the text version." 

# The HTML version
html = "Some text here. Use <br/> to insert newlines, or use most other basic HTML tags to format your message."

# MIME it up
mime_text = MIMEText(text, 'plain')
mime_html = MIMEText(html, 'html')

# Attach to the message
msg.attach(mime_text)
msg.attach(mime_html)

# Send it out
with smtplib.SMTP_SSL("smtp.gmail.com", 465) as smtp:
    smtp.login(secrets.secrets['gmail_login_address'], secrets.secrets['gmail_app_pass'])
    smtp.send_message(msg)
```

## Run on a schedule

To run this script on a schedule, it should ideally be converted to a Python (.py) file. You can accomplish this via command line as follows:

* prerequisites: `jupyter`, `nbconvert` (both installable via pip)

```
jupyter nbconvert --to script filename.ipynb
```

Now you have a script you can run using `cron`. You should edit the script to comment out the line that installs the prerequisites (it's near the beginning) so it doesn't try to install those prereqs every time the script is run. The converter also tries to be helpful by inserting a shebang at the top, but you should verify that the path will work when the file is executed by `cron`.

You will also likely need to make the resulting .py file executable. The alternative is to invoke it in cron by calling the full path to python first, then specifying the full path to your script.

Regardless of which method you choose, remember that wherever you're calling python from (system path or environment), you will need to have the `icalevents` Python library installed in that location.
