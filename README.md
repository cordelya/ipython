# iPython

General repository for assorted iPython/Jupyter notebooks

## Description
This repository is the designated spot for placing iPython/Jupyter notebooks when they aren't part of a larger project, or if there's only a small group of them.

## Fetching Data
Some of these notebooks fetch data from servers owned by others. Be a good neighbor and don't send too many requests at a time.

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation
These are meant to be used in a Jupyter Notebook or Jupyter Lab environment. Do:
```sh
python3 -m pip install jupyterlab
python3 -m jupyterlab
```
And then visit the url the service prints on your terminal window to interact with the notebooks

## Usage
Most of the notebooks have at least a minimal amount of documentation written in. You can read more about [iPython and Jupyter Lab](https://ipython.org/) at the project website.

## Support
For issues with iPython or Jupyter Lab, visit the link in the paragraph above. If you find an issue with a notebook itself, you can submit an issue in this project.

## Roadmap
No roads here. We go whichever way the wind blows.

## Contributing
This is a personal repository so I'm unlikely to accept contributions that aren't simple spelling/grammar/bug corrections. 

## License
This project is licensed under the MIT license. 

## Project status
This project gets new items when I think of things I might want to try out or look at. It's probably nothing groundbreaking, but on the other hand, you may find some examples of how to use some of the tools used in these notebooks.
